\documentclass[12pt, oneside, a4paper]{article}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{mathtools}
\usepackage{tikz}
\usetikzlibrary{cd}

% the following block is for illustrations
\usetikzlibrary{arrows}
\definecolor{wwwwww}{rgb}{0.4,0.4,0.4}
\definecolor{fftttt}{rgb}{1,0.2,0.2}
\definecolor{qqqqzz}{rgb}{0,0,0.6}
\definecolor{qqzzqq}{rgb}{0,0.6,0}
\definecolor{cqcqcq}{rgb}{0.75,0.75,0.75}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{fullpage}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{svg}

\newcommand{\ldbk}{\left[\!\left[}
\newcommand{\rdbk}{\right]\!\right]}

\newtheorem{theorem}{Théorème}[section]
\newtheorem{lemma}[theorem]{Lemme}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollaire}
\newtheorem{definition}[theorem]{Définition}
\newtheorem{example}[theorem]{Exemple}
\newtheorem{remark}{Remarque}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{proof}{Démonstration}[theorem]

\title{Autour des nombres de Catalan}
\author{stage 1A de recherche par Louis AUFFRET}
\date{22 mai 2023 - 24 juillet 2023}

\begin{document}

\begin{titlepage}
	\begin{center}
		\vspace*{5cm}

		\textbf{\huge Autour des nombres de Catalan}

		\vspace{0.5cm}

		Stage de recherche (1A) - rapport

		\vspace{0.5cm}

		22 mai 2023 - 24 juillet 2023	 

		\vspace{1.5cm}

		\textbf{Louis AUFFRET}

		\vfill

		\begin{figure}[!h]
			\centering
			\def\svgwidth{0.25\columnwidth}
			\input{logo_lamme.pdf_tex}
			\hspace{2.0cm}
			\def\svgwidth{0.5\columnwidth}
			\input{logo_ensiie.pdf_tex}
		\end{figure}

		\vspace{1.5cm}	 
	\end{center}
	\noindent
	\makebox[0pt][l]{Maître de stage : Abdelmejid BAYAD}%
	\makebox[\textwidth][c]{}%
	\makebox[0pt][r]{Tuteur du stage : Cyril BENEZET}
\end{titlepage}

\section*{Remerciements}

Je tiens avant tout à remercier
\begin{itemize}[label=\textbullet]
	\item Gilles Lacombe, qui m'a redirigé vers m. Bayad lorsque je lui ai fait part de ma volonté de faire mon stage dans la recherche en mathématiques;
	\item Abdelmejid Bayad, mon maître de stage, qui m'a permis de présenter à l'oral le fruit de mon travail au cours d'une conférence (Mediterranean International Conference of Pure \& Applied Mathematics, abrégée MICOPAM) qui s'est tenue dans le bâtiment IBGBI qui héberge le laboratoire, et qui propose de m'aider à formater les résultats que j'ai trouvés pendant et après mon stage, afin qu'ils soient publiables sous la forme d'un article de recherche;
	\item l'association Amarun, qui facilite aux étudiants Équatoriens en mathématiques l'accès à des études en Europe, association sans laquelle je n'aurais pas rencontré mes collègues de stage, qui sont de belles personnes;
	\item une maître de conférence dont je ne connais pas le nom (j'ai malheureusement oublié de le lui demander), avec qui une conversation durant une pause midi m'a aidé à réaliser ce que je voulais faire de mon avenir, et a probablement influencé mes futurs choix de vie;
	\item les enseignants/chercheurs/maîtres de conférences du laboratoire pour leur disponibilité et leur écoute.
\end{itemize}

\section*{Mots-clés}

\begin{itemize}[label=\textbullet]
	\item Recherche
	\item Mathématiques
	\item Combinatoire
	\item Algèbre
	\item Séries formelles
	\item Anneaux
	\item Coefficients binomiaux
	\item Catalan
	\item Fuss-Catalan
	\item Raney
\end{itemize}

\section*{Sur ce document}

Les termes techniques \textbf{en gras} sont de la terminologie mathématique officielle, les termes \underline{soulignés} sont de la terminologie inventée dans le cadre de ce stage car il est plus simple de donner un nom à une structure algébrique que d'énoncer ses propriétés à chaque fois qu'on la fait intervenir. Tous les anneaux dont il est question sont supposés unitaires, donc les morphismes d'anneaux envoient le neutre multiplicatif de leur source vers le neutre multiplicatif de leur but.

La partie 3 énonce des résultats déjà connus, les parties 4 et 5 sont des résultats que j'ai trouvés. Dans un souci de concision, leurs démonstrations ne figureront pas dans ce rapport.

\section*{Résumé}

Après une courte étude de la suite de Catalan et ses applications, je me suis très vite intéressé à ses généralisations : les suites de Fuss-Catalan (généralisation à 1 paramètre) et les suites de Raney (sur-généralisation à 2 paramètres). En examinant les démonstrations permettant de prouver que la suite de comptage de diverses classes combinatoires étant la suite de Catalan, j'ai remarqué une condition nécessaire et suffisante pour que ce soit bien la suite de Catalan, puis j'ai étendu ce théorème aux suites de Fuss-Catalan, donnant ainsi une interprétation générique des classes combinatoires associées et un moyen de construire des isomorphismes entre elles. Puis, ayant peu de notions d'analyse complexe, j'ai voulu donner donner des démonstrations purement algébriques aux expressions explicites de ces nombres, ce qui m'a amené à définir le minimum de structure algébrique permettant l'usage de coefficients binomiaux, démontrer des résultats usuels sur les coefficients binomiaux dans ce contexte, et me rendre compte que la démonstration de l'expression des nombres de Raney ainsi obtenue ne se limitait pas qu'à des paramètres entiers, et que ces nouvelles suites de Raney à paramètres non entiers respectaient les identités usuelles des suites de Raney.

\pagebreak

\tableofcontents

\pagebreak

\section{Introduction}

J'ai effectué mon stage au Laboratoire de Mathématiques et de Modélisation d'Évry (abrégé LaMME). Le laboratoire est divisé en plusieurs équipes, chacune ayant son domaine de recherche de prédilection. Mon sujet de recherche est ``Les nombres de Catalan''. Les nombres de Catalan sont une suite (infinie) de nombres entiers commençant par $1,1,2,5,14,42,132,\ldots$ qui intervient dans de nombreux contextes différents en combinatoire, lorsqu'il s'agit de compter le nombre d'éléments ``de taille $n$'' dans un certain ensemble. L'objectif était dans un premier temps de les étudier, les comprendre et constater ce qui lie les différents contextes dans lesquels ces nombres apparaissent, puis dans un second temps d'étudier ses différentes généralisations et des articles de recherche qui y sont liés, puis éventuellement trouver de nouveaux résultats. Bien entendu, ceci étant un stage, aucun nouveau résultat n'était exigible, mais était toujours bienvenu.

Après avoir présenté le laboratoire plus en détail, je donnerai la définition de la suite de Catalan, des suites de Fuss-Catalan (qui en sont une généralisation) et des suites de Raney (qui en sont une généralisation encore plus forte), je présenterai les contextes où on les retrouve en combinatoire, puis je mettrai en évidence un lien entre ces différents contextes en proposant une interprétation combinatoire générique de ces suites, puis j'y apporterai une définition étendue au-delà du contexte des nombres entiers qui conserve néanmoins les propriétés de ces suites.
\pagebreak



\section{Le laboratoire}

Le Laboratoire de Mathématiques et Modélisation d'Évry (LaMME) est hébergé aux étages 3 et 4 l'Institut de Biologie Génétique et Bioinformatique (IBGBI). Il est constitué de trois équipes et d'un membre hors-équipe :
\begin{itemize}[label=\textbullet]
	\item L'équipe Analyse et E.D.P. (équations aux dérivées partielles) se consacre à modéliser des phénomènes biologiques dans le but de les étudier et à l'étude de la mécanique des fluides par le biais de l'analyse fonctionnelle;
	\item L'équipe Probabilités et Mathématiques financières étudie des domaines tels que les équations différentielles stochastiques, l'apprentissage statistique (machine learning) ou l'évaluation de différents risques en finance;
	\item L'équipe Statistique et Génome est à l'interface de la biologie et des mathématiques (la composante mathématique y est cependant majoritaire), elle vise à faire usage de la statistique pour traiter des quantités de données biologiques immenses (séquences chromosomiques et/ou protéiques);
	\item Abdelmejid Bayad, algébriste, il s'agit de mon maître de stage.
\end{itemize}
Ceux qui travaillent dans les bureaux du LaMME sont enseignants-chercheurs, maîtres de conférences, doctorants ou stagiaires. Occasionnellement, il y est organisé des conférences et des séminaires. Par exemple, il y a régulièrement les séminaires des doctorants, où les doctorants présentent à l'oral leurs travaux en cours et répondent aux éventuelles questions d'autres doctorants et enseignants-chercheurs.
\pagebreak





\section{Nombres de Catalan, Fuss-Catalan et Raney : définitions et résultats connus}

\subsection{Classe combinatoire}

\begin{definition}
	Une \textbf{classe combinatoire} \cite{classes} est un ensemble $\mathcal A$ muni d'une application $|.| : \mathcal A \longrightarrow \mathbb N$, appelée \textbf{taille}, telle que $\forall n \in \mathbb N$, $\left\{a \in \mathbb A , \; |a| = n\right\}$ est fini. On note $\mathcal A_n \coloneqq \left\{a \in \mathcal A , \; |a| = n\right\}$ et la suite $\left(\#\mathcal A_n\right)_{n \in \mathbb N}$ est la \textbf{suite de comptage} de $\mathcal A$.
	Un produit cartésien de classes combinatoires $\mathcal A$ et $\mathcal B$ sera supposé muni de la somme des tailles de ses composants :
	\begin{equation*}
		\forall (a,b) \in \mathcal A \times \mathcal B, \; \left|(a,b)\right| \coloneqq |a|+|b|
	\end{equation*}
\end{definition}

Lorsque je parlais en introduction de différents ``contextes'' en combinatoire dans lesquels on retrouve les suites de Catalan, Fuss-Catalan et Raney, je voulais dire que ce sont les suites de comptages dans de très nombreux exemples de classes combinatoires. Les exemples suivants sont pour la plupart tirés de \textit{Nombres de Catalan} \cite{catalan}.

\subsection{Suite de Catalan}
\begin{definition}
	La \textbf{suite de Catalan} $(c_n)_{n \in \mathbb N}$ peut être définie de plusieurs façons, toutes équivalentes. \\
	On peut la définir ainsi :
	\begin{equation*}
		\left\{
			\begin{aligned}
				&c_0 \coloneqq 1 \\
				&\forall n \in \mathbb N, \;
				c_{n+1} \coloneqq \sum_{i=0}^n c_i \, c_{n-i}
				= \sum_{
					\begin{smallmatrix}
						i,j \in \mathbb N \\
						i+j=n
					\end{smallmatrix}
				} c_i c_j
			\end{aligned}
		\right.
	\end{equation*}
	ou par les séries
	\begin{equation*}
		\left\{
			\begin{aligned}
				&c_0 = 1 \\
				&\sum_{n \geq 0} c_{n+1} X^n = \left(\sum_{n \geq 0} c_n X^n \right)^2
			\end{aligned}
		\right.
	\end{equation*}
	ou encore une fois par les séries comme l'unique solution de cette équation
	\begin{equation*}
		X \left(\sum_{n \geq 0} c_n X^n \right)^2 = \left(\sum_{n \geq 0} c_n X^n \right) - 1
	\end{equation*}
	ou par une expression explicite :
	\begin{equation*}
		\forall n \in \mathbb N, \;
		c_n = \frac{1}{n+1} \binom{2n}{n} = \frac{1}{2 n+1} \binom{2n+1}{n}
	\end{equation*}
\end{definition}

Voyons à présent des exemples de classes combinatoires dont la suite de comptage est celle de Catalan.

\begin{example}
	Soit $n \in \mathbb N$. On définit un \textbf{chemin entier de $(0,0)$ à $(n,n)$ sous la première bissectrice} de taille comme étant un $2n+1$-uplet $M_0 M_1 \ldots M_{2n}$ de $\mathbb N^2$ tel que $M_0 = (0,0)$, $M_{2n} = (n,n)$ et $\forall i \in \ldbk 1, 2n \rdbk, \; M_i - M_{i-1} = (1,0)$ ou $(0,1)$. On considère $\mathcal A_n$ l'ensemble des chemins entiers de $(0,0)$ à $(n,n)$ sous la première bissectrice et on pose
	\begin{equation*}
		\mathcal A = \bigsqcup_{n \in \mathbb N} \mathcal A_n
	\end{equation*}
	Comme $\left(\mathcal A_n\right)_{n \in \mathbb N}$ est une partition de $\mathcal A$, on pose
	\begin{equation*}
		\forall n \in \mathbb N, \; \forall a \in \mathcal A_n, \;
		|a| \coloneqq n
	\end{equation*}
	$\left(\mathcal A, |.|\right)$ est alors une classe combinatoire et sa suite de comptage est la suite de Catalan.
	\begin{figure}[!h]
		\centering
		\input{../latex/illustrations/fig14_4.tex}
		\label{chemins}
		\caption{Les $c_3=5$ chemins de taille $3$}
	\end{figure}
\end{example}

\begin{example}
	Soit $n \in \mathbb N$. Si $n=0$ on définit $A_0 = \{\varnothing\}$ un singleton, sinon $\mathcal A_n$ l'ensemble des \textbf{triangulations d'un polygone convexe} à $n+2$ côtés, c'est-à-dire des ensembles d'arêtes que l'on peut attribuer à $\{s_1, \ldots, s_{n+2}\}$ tels que, représenté dans le plan, le graphe soit un polygone découpé en triangles par des diagonales non sécantes.
	\begin{figure}[!h]
		\centering
		\input{../latex/illustrations/fig14_6.tex}
		\label{polygones}
		\caption{Les $c_3=5$ triangulations d'un pentagone convexe}
	\end{figure}

	alors si on définit
	\begin{equation*}
		\mathcal A = \bigsqcup_{n \in \mathbb N} \mathcal A_n \text{ et } \forall n \in \mathbb N, \; \forall a \in \mathcal A_n, \; |a| \coloneqq n
	\end{equation*}
	alors $\left(\mathcal A, |.|\right)$ est une classe combinatoire et sa suite de comptage est la suite de Catalan.
\end{example}

\begin{example}
	Un \textbf{mot parenthésé} est un mot d'un alphabet comprenant deux symboles, la parenthèse ouvrante ``('' et la parenthèse fermante ``)''. Un \textbf{mot de Dyck} est un mot parenthésé qui comporte le même nombre de parenthèses ouvrantes et fermantes, et qui est également ``syntaxiquement correct'', c'est-à-dire que si on le tronque en ne prenant que ses premières parenthèses, on aura toujours plus de parenthèses ouvrantes que fermantes. \\
	Exemple : $(()(()()))((()))$ \\
	Contre-exemple : $())(()$ \\
	Si l'on considère $\mathcal A$ l'ensemble des mots de Dyck, et que la taille d'un mot de Dyck est la moitié de sa longueur, alors $\left(\mathcal A, |.|\right)$ est une classe combinatoire et sa suite de comptage est la suite de Catalan.
	\begin{figure}[!h]
		\centering
		\input{../latex/illustrations/fig14_5.tex}
		\label{dyck}
		\caption{Les $c_3=5$ mots de Dyck de longueur $6$}
	\end{figure}
\end{example}

\begin{example}
	Soient $\left(x_n\right)_{n \in \mathbb N}$ une suite de symboles représentant des éléments d'un ensemble $E$. $E$ est muni d'une opération $\star : E^2 \longrightarrow E$, à priori non associative. On construit $\mathcal A$ ainsi : $\forall n \in \mathbb N$, les éléments de $\mathcal A$ de taille $n$ sont les \textbf{parenthésages} possibles de l'expression $x_0 \star x_1 \star \ldots \star x_n$, c'est-à-dire les ordres dans lesquels les opérations peuvent être effectuées. Alors $\left(\mathcal A, |.|\right)$ est une classe combinatoire et sa suite de comptage est la suite de Catalan.
	\begin{figure}[!h]
		\centering
		\input{../latex/illustrations/fig14_3.tex}
		\label{parenthesages}
		\caption{Les $c_3=5$ parenthésages d'une expression à $4$ termes}
	\end{figure}
\end{example}

\begin{example}
	On pose $\Delta$ l'ensemble des mots binaires, on note $e$ le mot vide et on munit $\Delta$ de la relation d'ordre suivante : $a \preccurlyeq b$ si et seulement si $\exists c \in \Delta, \; b = ac$, avec $ac$ la concaténation de $a$ et de $c$. Un \textbf{arbre binaire} est une partie finie $T$ de $\Delta$ telle que $\forall s \in T$, $T$ contient tous les minorants de $s$, c'est-à-dire tous les mots obtenus en supprimant des bits à droite de $s$. Si on note $\mathcal A$ l'ensemble des arbres binaires et qu'on définit sur $\mathcal A$ la taille qui à chaque arbre binaire associe son cardinal, alors $\left(\mathcal A, |.|\right)$ est une classe combinatoire et sa suite de comptage est la suite de Catalan.
	\begin{figure}[!h]
		\centering
		\input{../latex/illustrations/fig14_1.tex}
		\label{arbresbinaires}
		\caption{Les $c_3=5$ arbres binaires à $3$ éléments (ou sommets)}
	\end{figure}
\end{example}

\begin{example}
	En reprenant les définitions de l'exemple précédent, un arbre binaire $T$ est dit \textbf{strict} si 
	\begin{equation*}
		\forall x \in T, \;
		(x 0 \in T \text{ et } x 1 \in T) \text{ ou } (x 0 \notin T \text{ et } x 1 \notin T)
	\end{equation*}
	et on dit d'un élément de $T$ que c'est une \textbf{feuille} de $T$ s'il n'a pas de majorant strict dans $T$.
	Si l'on considère $\mathcal A$ l'ensemble des arbres binaires stricts non vides, et que la taille d'un élément de $\mathcal A$ est son nombre de feuilles moins $1$, alors $\left(\mathcal A, |.|\right)$ est une classe combinatoire et sa suite de comptage est la suite de Catalan.
	\begin{figure}[!h]
		\centering
		\input{../latex/illustrations/fig14_2.tex}
		\label{arbresbinairesstricts}
		\caption{Les $c_3=5$ arbres binaires stricts à $4$ feuilles}
	\end{figure}
\end{example}

\subsection{Suites de Fuss-Catalan}
\begin{definition}
	Soit $p \in \mathbb N$. La \textbf{suite de Fuss-Catalan} de paramètre $p$, notée $\left(C_n^{(p)}\right)_{n \in \mathbb N}$ peut aussi être définie de plusieurs façons, toutes équivalentes. \\
	On peut la définir ainsi :
	\begin{equation*}
		\left\{
			\begin{aligned}
				&C_0^{(p)} \coloneqq 1 \\
				&\forall n \in \mathbb N, \;
				C_{n+1}^{(p)} \coloneqq \sum_{
					\begin{smallmatrix}
						i_1,\ldots,i_p \in \mathbb N \\
						i_1+\ldots+i_p = n
					\end{smallmatrix}
				} \prod_{j=1}^{p} C_{i_j}^{(p)}
			\end{aligned}
		\right.
	\end{equation*}
	ou par les séries
	\begin{equation*}
		\left\{
			\begin{aligned}
				&c_0 = 1 \\
				&\sum_{n \geq 0} C_{n+1}^{(p)} X^n = \left(\sum_{n \geq 0} C_n^{(p)} X^n \right)^p
			\end{aligned}
		\right.
	\end{equation*}
	ou encore une fois par les séries comme l'unique solution de cette équation
	\begin{equation*}
		X \left(\sum_{n \geq 0} C_n^{(p)} X^n \right)^p = \left(\sum_{n \geq 0} C_n^{(p)} X^n \right) - 1
	\end{equation*}
	ou par une expression explicite :
	\begin{equation*}
		\forall n \in \mathbb N, \;
		c_n = \frac{1}{p n+1} \binom{p n + 1}{n}
	\end{equation*}
	Notons que la suite de Catalan est la suite de Fuss-Catalan de paramètre $2$ : les suites de Fuss-Catalan sont donc bien une généralisation de la suite de Catalan.
\end{definition}

\subsection{Suites de Raney}
\begin{definition}
	Soit $(p,r) \in \mathbb N^2$. La \textbf{suite de Raney} de paramètres $(p,r)$, notée $\left(R_n^{(p,r)}\right)_{n \in \mathbb N}$ peut aussi être définie de plusieurs façons, toutes équivalentes. \\
	On peut la définir ainsi :
	\begin{equation*}
		\forall n \in \mathbb N, \;
		R_n^{(p,r)} \coloneqq \sum_{
			\begin{smallmatrix}
				i_1,\ldots,i_r \in \mathbb N \\
				i_1+\ldots+i_r = n
			\end{smallmatrix}
		}
		\prod_{j=1}^{r} C_{i_j}^{(p)}
	\end{equation*}
	ou par les séries
	\begin{equation*}
		\sum_{n \geq 0} R_n^{(p,r)} X^n = \left(\sum_{n \geq 0} C_n^{(p)} X^n\right)^r
	\end{equation*}
	ou par une expression explicite :
	\begin{equation*}
		\forall n \in \mathbb N, \;
		c_n = \frac{1}{p n+1} \binom{p n + 1}{n}
	\end{equation*}
	Notons que la suite de Fuss-Catalan de paramètre $p$ est la suite de Raney de paramètres $(p,1)$ : les suites de Raney sont donc bien une généralisation des suites de Fuss-Catalan.
\end{definition}

\begin{remark}
	On peut alors constater que si $\left(\mathcal A, |.|\right)$ est une classe combinatoire dont la suite de comptage est une suite de Fuss-Catalan avec un certain paramètre $p \in \mathbb N$, alors la classe $\mathcal A^r$ a pour suite de comptage $\left(R_n^{(p,r)}\right)$.
\end{remark}

\pagebreak




\section{Interprétation combinatoire générique}
\begin{definition}
	Soient $p \in \mathbb N$, $\mathcal A$ un ensemble, $|.| : \mathcal A \longrightarrow \mathbb N$ une application. $\forall n \in \mathbb N$ on note $\mathcal A_n \coloneqq \left\{a \in \mathcal A, \; |a|=n\right\}$. Alors si $\#\mathcal A_0 = 1$ et il existe sur $\mathcal A$ une opération $p$-aire $\psi : \mathcal A^p \to \mathcal A$ telle que
	\begin{equation*}
		\forall (a_1, \ldots, a_p) \in \mathcal A^p, \;
		\left|\psi(a_1, \ldots, a_p)\right|
		= 1 +\sum_{k=1}^p \left|a_k\right|
	\end{equation*}
	et
	\begin{equation*}
		\forall b \in \mathcal A \setminus \mathcal A_0, \;
		\exists !(a_1, \dots, a_p) \in \mathcal A^p, \;
		b = \psi(a_1, \ldots, a_p)
	\end{equation*}
	Alors on appellera le triplet $\left(\mathcal A, |.|, \psi\right)$ une \underline{$p$-classe}
\end{definition}

\begin{theorem}
	Soit $p \in \mathbb N$. Si $\left(\mathcal A, |.|, \psi\right)$ est une $p$-classe, alors $\left(\mathcal A,|.|\right)$ est une classe combinatoire et $\forall n \in \mathbb N, \; \#\mathcal A_n = C_n^{(p)}$

	Réciproquement, si $\left(\mathcal A, |.|\right)$ est une classe combinatoire telle que $\forall n \in \mathbb N, \; \#\mathcal A_n = C_n^{(p)}$, alors il  existe $\psi : \mathcal A^p \longrightarrow \mathcal A$ telle que $\left(\mathcal A, |.|, \psi\right)$ soit une $p$-classe.
\end{theorem}

\begin{corollary}
	Soient $p,r \in \mathbb N$. Si $\left(\mathcal A, |.|, \psi\right)$ est une $p$-classe, alors $\mathcal A^r$ a pour suite de comptage $\left(R_n^{(p,r)}\right)$.
\end{corollary}

\begin{definition}
	Soient $p \in \mathbb N$, $\left(\mathcal A, |.|_{\mathcal A}, \psi_{\mathcal A}\right)$ et $\left(\mathcal B, |.|_{\mathcal B}, \psi_{\mathcal B}\right)$ deux $p$-classes. On appelle \underline{morphisme de $p$-classes} de $\mathcal A$ dans $\mathcal B$ toute application de $\mathcal A$ dans $\mathcal B$ conservant la taille et l'opérateur $p$-aire, c'est-à-dire toute application $\varphi : \mathcal A \longrightarrow \mathcal B$ vérifiant :
	\begin{align*}
		&\forall a \in \mathcal A, \; \left|\varphi(a)\right|_{\mathcal B} = |a|_{\mathcal A} \\
		&\forall (a_1,\ldots,a_p) \in \mathcal A^p, \; \varphi\left(\psi_{\mathcal A}(a_1,\ldots,a_p)\right) = \psi_{\mathcal B}\left(\varphi(a_1),\ldots,\varphi(a_p)\right)
	\end{align*}
\end{definition}

\begin{theorem}
	Soient $p \in \mathbb N$, $\left(\mathcal A, |.|_{\mathcal A}, \psi_{\mathcal A}\right)$ et $\left(\mathcal B, |.|_{\mathcal B}, \psi_{\mathcal B}\right)$ deux $p$-classes. Il existe un unique morphisme de $p$-classes de $\mathcal A$ dans $\mathcal B$. On le note $\varphi_{\mathcal A, \mathcal B}$.
\end{theorem}

\begin{corollary}
	Soit $p \in \mathbb N$. Toutes les $p$-classes sont isomorphes deux à deux, car deux $p$-classes $\mathcal A$ et $\mathcal B$ vérifient
	\begin{align*}
		\varphi_{\mathcal B, \mathcal A} \circ \varphi_{\mathcal A, \mathcal B} &= \varphi_{\mathcal A, \mathcal A} = \mathrm{id}_{\mathcal A} \\
		\varphi_{\mathcal A, \mathcal B} \circ \varphi_{\mathcal B, \mathcal A} &= \varphi_{\mathcal B, \mathcal B} = \mathrm{id}_{\mathcal B}
	\end{align*}
	et l'isomorphisme de $p$-classes entre $\mathcal A$ et $\mathcal B$ est unique.
\end{corollary}

\begin{remark}
	Sur des classes combinatoires ayant une suite de Fuss-Catalan comme suite de comptage, définir des structures de $p$-classes est une façon de construire une bijection entre chacune de ces classes. De plus, la composition de morphismes étant un morphisme, on a $\varphi_{\mathcal B, \mathcal C} \circ \varphi_{\mathcal A, \mathcal B} = \varphi_{\mathcal A, \mathcal C}$.
\end{remark}


\pagebreak




\section{Suite de Raney à paramètres non-entiers : définition étendue}

\subsection{Les anneaux binomiaux}
L'objectif de cette partie est d'étendre la définition des suites de Raney à des paramètres non-entiers. Je me suis alors demandé dans quel type d'espace je devais choisir les paramètres pour que les résultats prouvés dans cette partie soient vérifiés. Après avoir trouvé une définition des coefficients binomiaux généralisés \cite{concrete} $\binom{z}{n}$ avec $z \in \mathbb C$ et $n \in \mathbb Z$, j'ai donc fait l'inventaire des hypothèses sur la structure algébrique de $\mathbb C$ requises pour que la définition donnée à ces coefficients aie du sens, et je n'ai gardé que ces hypothèses, ce qui m'a amené à définir la structure suivante :

\begin{definition}
	On appellera ici un \underline{anneau binomial} tout anneau $\mathbb A$ commutatif unitaire contenant un sous-anneau isomorphe à $\mathbb Q$. Ou de façon équivalente, s'il existe un morphisme d'anneaux de $\mathbb Q$ dans $\mathbb A$ (il est nécessairement injectif car $\mathbb Q$ est un corps).
\end{definition}

\begin{remark}
	Dans un anneau binomial $\mathbb A$ le morphisme de $\mathbb Q$ dans $\mathbb A$ est unique, car l'image de $1$ est $1_{\mathbb A}$, l'image de $1$ détermine les images de tous les entiers naturels (qui sont des sommes de $1$), qui déterminent les images des entiers relatifs (par passage à l'opposé), qui déterminent les images des rationnels (par passage à l'inverse et produit). On peut donc utiliser un léger abus de notation en considérant les rationnels comme leur propre image par ce morphisme, et ainsi considérer $\mathbb Q \subset \mathbb A$.
\end{remark}

\begin{definition}
	Soit $\mathbb A$ un anneau binomial, on définit alors les \textbf{coefficients binomiaux} ainsi :
	\begin{equation*}
		\forall \alpha \in \mathbb A, \;
		\forall n \in \mathbb Z, \;
		\binom{\alpha}{n} \coloneqq  \left\{
			\begin{aligned}
				&\frac{\alpha^{\underline{n}}}{n!} \text{ si } n \geq 0 \\
				&0 \text{ sinon}
			\end{aligned}
		\right.
	\end{equation*}
	où $\alpha^{\underline{n}}$ désigne la \textbf{factorielle descendante} $\displaystyle \prod_{i=0}^{n-1} (z - i)$.
\end{definition}

À présent, on peut constater que les hypothèses qui correspondent à un anneau binomial sont suffisantes pour conférer aux séries entières à coefficients dans un anneau binomial des propriétés intéressantes, on peut notamment définir la dérivation et l'intégration en $0$ d'une série formelle, ainsi que d'autres concepts liés aux séries formelles que l'on trouve dans \textit{Principes de combinatoire classique} \cite{combinatorics} :

\begin{definition}
	Soient $\mathbb A$ un anneau binomial, $A = \sum_{n \geq 0} a_n X^n, B = \sum_{n \geq 0} b_n X^n \in \mathbb A[[X]]$ deux séries formelles.
	\begin{align*}
		&A' \coloneqq  \sum_{n \geq 0} (n+1) a_{n+1} X^n \\
		&\mathrm{Int}_0 (A) \coloneqq  \sum_{n \geq 1} \frac{a_{n-1}}{n} X^n \\
		&A \circ B = \sum_{n \geq 0} a_n B^n \text{ si cette famille est sommable}
	\end{align*}
	et on vérifie sans problème les indentités usuelles
	\begin{align*}
		&(A \cdot B)'  = A' \cdot B + A \cdot B' \\
		&\left(\mathrm{Int}_0 (A)\right)' = A \\
		&\text{si } o(B) \geq 1, \, (A \circ B)' = (A' \circ B) \cdot B'
	\end{align*}
	et $A$ possède un inverse (pour $\cdot$), noté $A^{-1}$ si et seulement si son terme constant (i.e. d'indice $0$) est inversible, et un \textbf{réverse} (inverse pour $\circ$), noté $A^{[-1]}$ si son terme constant est nul et son terme devant $X^1$ est inversible.
\end{definition}

\begin{definition}
	On pose
	\begin{equation*}
		\forall \alpha \in \mathbb A, \;
		\prescript{\alpha}{}(1+X) \coloneqq \sum_{n \geq 0} \binom{\alpha}{n} X^n
	\end{equation*}
	et pour toute série de $V \in \mathbb A[[X]]$ de terme constant $1$, on pose
	\begin{equation*}
		\prescript{\alpha}{}V \coloneqq \prescript{\alpha}{}(1+X) \circ \left(V-1\right)
	\end{equation*}
	Notons qu'il n'y a pas de conflit de notation entre ces deux définitions, la deuxième ne fait qu'étendre la première
\end{definition}

\begin{theorem}
	Soient $A,B \in \mathbb A[[X]]$ de terme constant $1$. $\forall n \in \mathbb Z, \; \prescript{n}{}A = A^n$, cette notation étend les puissances entières de $A$ donc à partir de maintenant on la notera $\forall \alpha \in \mathbb A, \; A^\alpha \coloneqq \prescript{\alpha}{}A$, et toutes les identités usuelles des puissances sont vérifiées :
	\begin{align*}
		&\forall (\alpha, \beta) \in \mathbb A, \; V^\alpha V^\beta = V^{\alpha+\beta} \\
		&\forall (\alpha, \beta) \in \mathbb A, \; \left(V^\alpha\right)^\beta = V^{\alpha\beta} \\
		&\forall \alpha \in \mathbb A, \; A^\alpha B^\alpha = \left(AB\right)^\alpha \\
		&\forall \alpha \in \mathbb A, \; \left(A^\alpha\right)' = \alpha A' \cdot A^{\alpha-1}
	\end{align*}
\end{theorem}

\begin{proposition}
	La formule de réversion de Lagrange-Bürmann dans le cadre des séries formelles \cite{combinatorics} est valide dans la cadre des séries formelles à coefficients dans un anneau binomial. En effet, la démonstration fournie dans \cite{combinatorics} ne fait appel qu'à des hypothèses vraies dans le cadre d'un anneau binomial.
\end{proposition}

\subsection{Suites de Raney généralisées}

On rappelle que $\forall p \in \mathbb N$, la suite de Fuss-Catalan de paramètre $p$ est définie par
\begin{align*}
	F = \sum_{n \geq 0} C_n^{(p)} X^n \iff X F^p = F-1
\end{align*}
On remarque que $X F^p$ a toujours un terme constant nul quelque soit la définition donnée à $F^p$, donc toute solution $F$ de cette équation a un terme constant égal à $1$, ce qui permet de définir les puissances non-entières de $F$ et d'étudier cette équation, dans le cas où $p \in \mathbb A$ un anneau binomial. On résout alors cette équation dans $\mathbb A[[X]]$ :
\begin{align*}
	F = \sum_{n \geq 0} C_n^{(p)} X^n &\iff X F^p = F-1 \\
	&\iff X\left(1+U\right)^p = U \text{ avec } U=F-1 \\
	&\iff X = U \left(1+U\right)^{-p} \\
	&\iff X = G \circ U \text{ avec } G = X \left(1+X\right)^{-p} \\
	&\iff G^{\left[-1\right]} = U \text{ car } G \text{ est réversible}\\
	&\iff F = 1+G^{\left[-1\right]}
\end{align*}
$F$ étant de terme constant $1$, définir $F^r$ avec $r \in \mathbb A$ ne pose aucun problème. Comme la suite de Raney de paramètres $(p,r) \in \mathbb N^2$ est définie ainsi :
\begin{equation*}
	\sum_{n \geq 0} R_n^{(p,r)} X^n \coloneqq \left(\sum_{n \geq 0} C_n^{(p)} X^n\right)^r = \left(1 + \left(X\left(1+X\right)^{-p}\right)^{\left[-1\right]}\right)^r
\end{equation*}
mais que cette expression a aussi du sens dans le cadre de paramètres $(p,r) \in \mathbb A$, ce qui permet d'étendre la définition.

\begin{definition}
	Soient $\mathbb A$ un anneau binomial, $(p,r) \in \mathbb A^2$, on définit alors la suite de Raney de paramètres $(p,r)$ ainsi :
	\begin{equation*}
		\sum_{n \geq 0} R_n^{(p,r)} X^n \coloneqq \left(1 + \left(X\left(1+X\right)^{-p}\right)^{\left[-1\right]}\right)^r
	\end{equation*}
\end{definition}

\begin{theorem}
	L'expression générale des nombres de Raney de paramètres $p, r$ quelconques dans $\mathbb A$ est la suivante :
	\begin{equation*}
		\forall p, r \in \mathbb A, \;
		\forall n \in \mathbb N, \;
		R_n^{(p,r)} = \left\{
			\begin{aligned}
				&1
				&\text{ si } n = 0 \\
				&\frac{r}{n} \binom{p n+r-1}{n-1}
				&\text{ si } n \geq 1
			\end{aligned}
		\right.
	\end{equation*}
	\newpage
\end{theorem}

L'avantage de définir ces suites à l'aide de séries formelles est que si trouver une expression explicite est la partie la plus difficile, trouver des identités est la partie la plus simple.

\begin{proposition}
	On a, quelque soient $p, r, r' \in \mathbb A, \; n \in \mathbb N$, les identités sur les séries ainsi que leurs équivalents sur les coefficients :
\begin{align*}
	&\text{Égalités sur des séries}
	&&&\text{Égalités }\forall n \in \mathbb N \\
	F^{\left(p, r\right)} F^{\left(p, r'\right)}
	&= F^{\left(p, r+r'\right)}
	&&\iff&
	\sum_{k=0}^n R_k^{\left(p, r\right)} R_{n-k}^{\left(p, r'\right)}
	&= R_n^{\left(p, r+r'\right)} \\
%
	X F^{\left(p, p\right)}
	&= F^{\left(p, 1\right)} - 1
	&&\iff&
	R_n^{\left(p, p\right)}
	&= R_{n+1}^{\left(p, 1\right)} \\
%
	\left(F^{\left(p, r\right)}\right)^{r'}
	&= F^{\left(p, r r'\right)}
	&&\iff&
	\sum_{k=0}^n \sum_{i=0}^k \binom{r'}{k} \binom{k}{i} \left(-1\right)^{k-i} R_n^{\left(p, i r\right)}
	&= R_n^{\left(p, r r'\right)} \\
%
	\left(F^{\left(p, r\right)}\right)'
	&= r \left(F^{\left(p, 1\right)}\right)' F^{\left(p, r-1\right)}
	&&\iff&
	\sum_{k=0}^n \left(k+1\right) R_{k+1}^{\left(p, 1\right)} R_{n-k}^{\left(p, r-1\right)}
	&= \left(n+1\right) R_{n+1}^{\left(p, r\right)} \\
%
	F^{\left(0, r\right)}
	&= \left(1+X\right)^r
	&&\iff&
	R_n^{\left(0, r\right)}
	&= \binom{r}{n} \\
%
	F^{\left(p, 0\right)}
	&= 1
	&&\iff&
	R_n^{\left(p, 0\right)}
	&= \delta_{0, n}
\end{align*}
\end{proposition}

\pagebreak

\section{Conclusion}

Ce stage de 2 mois dans la recherche m'a permis d'apporter aux classes combinatoires une condition nécessaire et suffisante pour que leur suite de comptage soit de Fuss-Catalan, en montrant qu'il est possible de définir une structure supplémentaire sur une telle classe et que réciproquement, si une telle structure existe sur cette classe, sa suite de comptage est de Fuss-Catalan. En déinissant une telle structure sur deux de ces classes, on trouve naturellement un isomorphisme entre les deux.

Les suites de Raney étant une généralisation des suites de Fuss-Catalan, j'y ai apporté une généralisation plus vaste encore, en prenant une des nombreuses définitions équivalentes des suites de Raney et en montrant que les paramètres n'ont pas besoin d'être entiers pour que ces suites soient définies, les paramètres ont seulement besoin d'être pris dans un espace muni d'une certaine structure algébrique. J'y ai également apporté une démonstration purement algébrique par les séries formelles, i.e. ne faisant pas appel à des notions d'analyse complexe.

Il est prévu que je rédige tous ces résultats au propre (et plus encore, les résultats présentés ici ne sont que ceux que j'ai trouvés durant mon stage) afin de les faire publier dans un atricle de recherche. N'étant pas très familier avec le formatage, les codes et normes d'un article de recherche, m. Bayad propose de relire mon travail et m'aider à le formatter correctement.







\pagebreak
\begin{thebibliography}{20}
	\addcontentsline{toc}{section}{\protect\numberline{}Références}
	\bibitem{catalan}
		\href{https://lescoursdemathsdepjh.monsite-orange.fr/file/ea7f2cae62471c48a60df51728fa8242.pdf}{Pierre-Jean Hormière - Nombres de Catalan}
	\bibitem{concrete}
		\href{https://www.csie.ntu.edu.tw/~r97002/temp/Concrete\%20Mathematics\%202e.pdf}{Ronald L. Graham, Donald E. Knuth, Oren Patashnik - Concrete Mathematics} (chapitre 5 et chapitre 7 partie 5)
	\bibitem{combinatorics}
		\href{https://irma.math.unistra.fr/~foata/AlgComb.pdf}{Dominique Foata, Guo-Niu Han - Principes de combinatoire classique} (chapitres 1 à 7)
	\bibitem{classes}
		\href{https://www.labri.fr/perso/duchon/Enseignements/Probas/cours3.pdf}{Philippe Duchon - Probabilités, Statistiques, Combinatoire}
	\bibitem{motzkin}
		\href{https://arxiv.org/pdf/2109.05291.pdf}{Irena Rusu - Raney numbers, threshold sequences and Motzkin-like paths}
	\bibitem{fusscatalan}
		\href{https://arxiv.org/pdf/0711.0906.pdf}{Jean-Christophe Aval - Multivariate Fuss-Catalan Numbers}
	\bibitem{raney}
		\href{https://www.ams.org/journals/tran/1960-094-03/S0002-9947-1960-0114765-9/S0002-9947-1960-0114765-9.pdf}{George N. Raney - Functional composition patterns and series reversion}
\end{thebibliography}
\pagebreak
\section*{Glossaire}
\addcontentsline{toc}{section}{\protect\numberline{}Glossaire}
Rappel : les termes \textbf{en gras} sont officiels, les termes \underline{soulignés} sont inventés dans le cadre du stage.
\begin{itemize}
	\item \textbf{classe combinatoire} \& \textbf{suite de comptage} - définition page 6
	\item \textbf{suite de Catalan} - définition page 6
	\item \textbf{chemin entier sous la première bissectrice} - définition page 7
	\item \textbf{triangulation d'un polygone convexe} - définition page 7
	\item \textbf{mot de Dyck} - définition pages 7-8
	\item \textbf{arbre binaire} - définition page 8
	\item \textbf{suites de Fuss-Catalan} - définition page 9
	\item \textbf{suites de Raney} - définition pages 9-10
	\item \underline{$p$-classe} - définition page 11
	\item \underline{morphisme de $p$-classes} - définition page 11
	\item \underline{anneau binomial} - définition page 12
	\item \textbf{coefficient binomial} - définition généralisée page 12
	\item \textbf{factorielle descendante} - définition généralisée page 12
	\item \textbf{réverse, réversibilité} - inversibilité pour la composition $\circ$, terminologie utilisée dans \textit{Principes de combinatoire classique} \cite{combinatorics} - page 13
\end{itemize}

\pagebreak
\section*{Annexes}
\addcontentsline{toc}{section}{\protect\numberline{}Annexes}

\subsection*{Le développement durable au LaMME}
\addcontentsline{toc}{subsection}{\protect\numberline{}Le développement durable au LaMME}
Pour dresser un inventaire des efforts mis en place par le laboratoire dans le cadre du développement durable, je me suis entretenu avec Lucilla Corrias, qui fait partie du comité Développement Durable du laboratoire, qui a été créé récemment. Ce comité, avec la collaboration d'Anne-Laure Ligozat, a inscrit le laboratoire sur la plateforme de \href{https://labos1point5.org/}{\textit{Labos 1 point 5}}, un collectif de chercheurs dont le but est de sensibiliser les chercheurs au développement durable. La plateforme Labos 1 point 5 permet de dresser un bilan carbone d'un laboratoire sur la durée d'une année. Ce bilan carbone est construit en utilisant des données telles que les moyens de transport utilisés par les chercheurs, leur fréquence et distance de déplacement, les dépenses annuelles du laboratoire en matériel informatique, en gaz et en éléctricité (y compris l'usage des serveurs de calcul mis à disposition notamment de l'équipe Statistique et Génome).

Des trottinettes fournies par le laboratoire et des vélos fournis par l'Université sont également disponibles à l'emprunt pour les membres du laboratoire.

Seul bémol : un effort doit être fait vis-à-vis du tri sélectif, le laboratoire n'étant pas muni de poubelles dédiées aux déchets recyclables.

%\subsection*{Démonstrations}
%\addcontentsline{toc}{subsection}{\protect\numberline{}Démonstrations}

\end{document}